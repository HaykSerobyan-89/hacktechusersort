from faker import Faker
import numpy as np
import json
import csv


def create_fake_users(count=1000):
    fake_users = []
    for i in range(count):
        fake = Faker()
        fake_users.append(
            {
                "id": i + 1,
                "name": fake.first_name(),
                "last_name": fake.last_name(),
                "mail": fake.email(),
                "status": np.random.choice(["Active", "Pending", "Canceled", "Suspended"])
            }
        )
    return fake_users


def add_data_to_json(data):
    with open("users.json", "a+") as file:
        file.write(json.dumps(data))


def write_row_in_csv(status, *info):
    with open(f'SortedData/{status}.csv', 'a+', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow([*info, status])
