from fake_users import create_fake_users, add_data_to_json, write_row_in_csv
import json

data = create_fake_users(count=2000)
add_data_to_json(data)

with open("users.json", "r") as file:
    data = json.load(file)
    for user in data:
        write_row_in_csv(
            user.get("status"),
            user.get("id"),
            user.get("name"),
            user.get("last_name"),
            user.get("mail")
        )
